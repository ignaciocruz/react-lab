import React from "react";
import { Link } from "react-router-dom";

const HomePage = () => (
  <div className="jumbotron">
    <h1>Administración de Artículos</h1>
    <p>Añada y visualice artículos de su tienda</p>
    <Link to="viewArticlesPage" className="btn btn-primary btn-lg">
      Ver Artículos
    </Link>
  </div>
);

export default HomePage;
