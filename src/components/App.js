import React from "react";
import { Route } from "react-router-dom";
import HomePage from "./home/HomePage";
import AboutPage from "./about/AboutPage";
import AddArticlePage from "./articles/AddArticlePage";
import ViewArticlesPage from "./articles/ViewArticlesPage";
import Header from "./common/Header";


function App() {
  return (
    <div className="container-fluid">
      <Header />
      <Route exact path="/" component={HomePage} />
      <Route path="/addArticlePage" component={AddArticlePage} />
      <Route path="/viewArticlesPage" component={ViewArticlesPage} />

    </div>
  );
}

export default App;
