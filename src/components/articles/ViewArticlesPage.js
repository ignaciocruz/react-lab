import React from "react";
import firebase from "../../Firebase";

class ViewArticlesPage extends React.Component {

  db = firebase.firestore().collection('articles');
  unsubscribe = null;
  state = { articles: [] };
  // Works also with componentDidMount but slightly slower
  componentWillMount() {
    const articles = []
    
    this.db.get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        const { name, quantity, description, price } = doc.data();
        articles.push({
          key: doc.id,
          name,
          quantity,
          description,
          price
        });
      });

      this.setState({ articles });
    });


  }



  render() {
    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">
              Ver artículos
            </h4>
          </div>
          <div className="panel-body">
            <table className="table table-stripe">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Quantity</th>
                </tr>
              </thead>
              <tbody>
                {this.state.articles.map(article =>
                  <tr key={article.key}>
                    <td>{article.name}</td>
                    <td>{article.description}</td>
                    <td>{article.price}</td>
                    <td>{article.quantity}</td>
                  </tr>
                )}
              </tbody>
            </table>

          </div>
        </div>
      </div>
    )
  }
}

export default ViewArticlesPage;