import React from "react";
import firebase from "../../Firebase";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



class AddArticlePage extends React.Component {

  db = firebase.firestore().collection('articles');


  state = {
    article: {
      name: "",
      quantity: "",
      description: "",
      price: ""
    }
  };

  handleChange = event => {
    const article = {
      ...this.state.article,
      [event.currentTarget.name]: event.currentTarget.value
    };
    this.setState({ article });
  };

  handleSave = event => {
    event.preventDefault();

    const { name, quantity, description, price } = this.state.article;

    this.db.add({
      name,
      quantity,
      description,
      price
    }).catch((error) => {
      toast.error("Error al añadir artículo");
      console.error("Error adding article:", error);
    });

    this.setState({
      name: "",
      quantity: "",
      description: "",
      price: ""
    });

    toast.success("¡Artículo añadido!")

  };


  render() {
    return (
      <form onSubmit={this.handleSave}>
        <h4>Añadir artículo</h4>

        <div className="form-group">
          <label htmlFor="name">Nombre:</label>
          <div className="field">
            <input
              name="name"
              type="text"
              onChange={this.handleChange}
              value={this.state.article.name}
            />
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="quantity">Cantidad</label>
          <div className="field">
            <input
              name="quantity"
              type="text"
              onChange={this.handleChange}
              value={this.state.article.quantity}
            />
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="description">Descripción:</label>
          <div className="field">
            <textarea
              name="description"
              type="text"
              onChange={this.handleChange}
              value={this.state.article.description}
            />
          </div>
        </div>


        <div className="form-group">
          <label htmlFor="price">Precio:</label>
          <div className="field">
            <input
              name="price"
              type="text"
              onChange={this.handleChange}
              value={this.state.article.price}
            />
          </div>
        </div>


        <input type="submit" value="Añadir" />
        <ToastContainer />
      </form>
    );
  }
}



export default AddArticlePage;
