import React from "react";
import { NavLink } from "react-router-dom";

const Header = () => {
  const activeStyle = { color: "#F15B2A" };
  return (
    <nav>
      <NavLink to="/" activeStyle={activeStyle} exact>
        Inicio
      </NavLink>
      {" | "}
      <NavLink to="/addArticlePage" activeStyle={activeStyle}>
        Añadir Artículo
      </NavLink>
      {" | "}
      <NavLink to="/viewArticlesPage" activeStyle={activeStyle}>
        Ver Artículos
      </NavLink>
    </nav>
  );
};

export default Header;
