import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const config = {
    apiKey: "AIzaSyD81FHKMFbQaGRvovUyC8xiy38O2QaJrOA",
    authDomain: "sublime-state-253916.firebaseapp.com",
    databaseURL: "https://sublime-state-253916.firebaseio.com",
    projectId: "sublime-state-253916",
    storageBucket: "sublime-state-253916.appspot.com",
    messagingSenderId: "626033521977",
    appId: "1:626033521977:web:bf7d88f46ea24f74bee4e8",
    measurementId: "G-C5NJ7RG204"
};

firebase.initializeApp(config);


export default firebase;